import { 
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink,
    Container,
    Button,
} from 'reactstrap';
import logo from "../assets/images/logo.png";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {  faShoppingCart } from '@fortawesome/free-solid-svg-icons';
import { faFacebook, faTwitter, faInstagram } from '@fortawesome/free-brands-svg-icons';
import { useEffect, useState } from 'react';
import ShopBreadcrump from './AppBreadcrumb';
import GoogleIcon from '@mui/icons-material/Google';
import { useSelector } from 'react-redux';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import auth from '../firebase';
import { onAuthStateChanged, GoogleAuthProvider, signOut } from 'firebase/auth';
import { signInGoogleAccountSuccessful } from '../actions/action';
import { useDispatch } from 'react-redux';
import { signOutGoogleAccount } from '../actions/action';


export default function Navigator () {
    const dispatch = useDispatch();
    const {userGoogleAccount} = useSelector((data) => data.productReducer);
    //navbar
    const [isOpen, setIsOpen] = useState(false);
    const toggle = () => setIsOpen(!isOpen);

    const [anchorEl, setAnchorEl] = useState(null);
    const open = Boolean(anchorEl);
    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };
    const handleClose = () => {
        setAnchorEl(null);
    };
    const handleLogOut = () => {
        signOut(auth)
        .then(() => {
            dispatch(signOutGoogleAccount())
            setAnchorEl(null)
        }
        ).catch((error) =>{
            console.log(error)
            setAnchorEl(null)
        }
        )
    }
    useEffect(() => {
        console.log(userGoogleAccount);
        onAuthStateChanged (auth,  (result) => {
          if(result){
            dispatch(signInGoogleAccountSuccessful(result));

          }else{
            dispatch(signInGoogleAccountSuccessful({}))
          }
        })
      }, []) 
    return(
        <>
            <Container>
                <Navbar  fixed="top" expand='md'  dark= {true}  className="container" id='nav1'>
                    <NavbarBrand href="/">
                        <img src={logo} alt="logo" className='logo'/>
                    </NavbarBrand>
                    <NavbarToggler onClick={toggle} />
                    <Collapse isOpen={isOpen}  navbar  >
                        <Nav  navbar  className='w-100 hearder-link-position' horizontal='end'>
                            <NavItem>
                                <NavLink href="/" className='headerLink mx-3'><span>Men</span></NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink href="/" className='headerLink mx-3'><span>Woman</span></NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink href="/" className='headerLink mx-3'><span>Child</span></NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink href="/"  className='headerLink mx-3'><span>Collection</span></NavLink>
                            </NavItem>
                        </Nav>
                        <Nav  navbar   className='w-50 ' horizontal='end'>
                            {
                               JSON.stringify(userGoogleAccount) === JSON.stringify({}) ? 
                                <>
                                    <NavItem>
                                        <NavLink href="/"><span className='mx-3'><FontAwesomeIcon icon={faFacebook} size="lg"/></span></NavLink>
                                    </NavItem>
                                    <NavItem>
                                        <NavLink href="/Login"><span className='mx-3'><GoogleIcon/></span></NavLink>
                                    </NavItem>
                                    <NavItem>
                                        <NavLink href="/"><span className='mx-3'><FontAwesomeIcon icon={faTwitter} size="lg"/></span></NavLink>
                                    </NavItem>
                                    <NavItem>
                                        <NavLink href="/"><span className='mx-3'><FontAwesomeIcon icon={faShoppingCart} size="lg"/></span></NavLink>
                                    </NavItem>
                                </>
                                :
                                <>
                                     <NavItem>
                                        <NavLink href="/"><code>{userGoogleAccount === { hello: "Hello"}}</code></NavLink>
                                    </NavItem>
                                    <NavItem>
                                        <img src={userGoogleAccount.photoURL} style={{
                                                width:"40px", 
                                                borderRadius: "50%"
                                        }}/>
                                    </NavItem>
                                    <NavItem>
                                        <Button 
                                                style={{ 
                                                    backgroundColor:"transparent",
                                                    border:"none", 
                                                    position:"relative",
                                                    top:"3px"
                                                }}
                                                aria-controls={open ? 'basic-menu' : undefined}
                                                aria-haspopup="true"
                                                aria-expanded={open ? 'true' : undefined}
                                                onClick={handleClick}
                                            >
                                                <span>{userGoogleAccount.displayName}</span>
                                        </Button>
                                        <Menu
                                            id="basic-menu"
                                            anchorEl={anchorEl}
                                            open={open}
                                            onClose={handleClose}
                                            MenuListProps={{
                                            'aria-labelledby': 'basic-button',
                                            }}
                                        >
                                            <MenuItem >Profile</MenuItem>
                                            <MenuItem >My account</MenuItem>
                                            <MenuItem onClick={handleLogOut}>Logout</MenuItem>
                                        </Menu>
                                    </NavItem>
                                </> 
                            }
                        </Nav>
                    </Collapse>
                </Navbar>
            </Container>
            <ShopBreadcrump></ShopBreadcrump>
        </>
    )
}