import { 
    SEND_REQUEST_TAKE_PRODUCT_PENDING,
    SEND_REQUEST_TAKE_PRODUCT_START,
    SEND_REQUEST_TAKE_PRODUCT_END, 
    SEND_AUTHENTICATION_TO_GOOGLE_PENDING,
    SEND_AUTHENTICATION_TO_GOOGLE_SUCCESS, 
    SEND_AUTHENTICATION_TO_GOOGLE_END, 
    SIGN_OUT_GOOGLE_ACCOUNT
 } from "../constant/constant";




const initialState = {
    products: [],
    userGoogleAccount: {
       
    }
};

const productReducer = (state = initialState, action) => {
    switch (action.type){
        case SEND_REQUEST_TAKE_PRODUCT_PENDING:
            break;
        case SEND_REQUEST_TAKE_PRODUCT_START:
            state.products = action.data.data;
            break;
        case SEND_REQUEST_TAKE_PRODUCT_END:
            break;
        default:
            break;
        case SEND_AUTHENTICATION_TO_GOOGLE_PENDING:
            break;
        case SEND_AUTHENTICATION_TO_GOOGLE_SUCCESS:
            state.userGoogleAccount = action.payload;
            console.log(state.userGoogleAccount)
            break;
        case SEND_AUTHENTICATION_TO_GOOGLE_END:
            state.userGoogleAccount = {}
            break;
        case SIGN_OUT_GOOGLE_ACCOUNT:
            state.userGoogleAccount = {};
            break;
    }
    return{...state};
}

export default productReducer