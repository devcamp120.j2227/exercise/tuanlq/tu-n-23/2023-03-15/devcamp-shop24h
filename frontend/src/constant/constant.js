export const SEND_REQUEST_TAKE_PRODUCT_PENDING = "Bắt đầu lấy dữ liệu product từ database";

export const SEND_REQUEST_TAKE_PRODUCT_START = "Lấy dữ liệu product thành công";

export const SEND_REQUEST_TAKE_PRODUCT_END = "Lấy dữ liệu product thất bại";

export const SEND_AUTHENTICATION_TO_GOOGLE_PENDING = "Bắt đầu lấy dữ liệu user google account ";

export const SEND_AUTHENTICATION_TO_GOOGLE_SUCCESS = "Lấy dữ liệu google acount thành công";

export const SEND_AUTHENTICATION_TO_GOOGLE_END = "Lấy dữ liệu google account thất bại";

export const SIGN_OUT_GOOGLE_ACCOUNT = "Log out google account";
