import DefaultLayout from "./layout/DefaultLayout";
import "bootstrap/dist/css/bootstrap.min.css";
import "../src/App.css";
import { library } from '@fortawesome/fontawesome-svg-core';
import { faB } from "@fortawesome/free-solid-svg-icons";
import { faCheckSquare, faCoffee } from '@fortawesome/free-solid-svg-icons';


library.add(faB, faCheckSquare, faCoffee)
function App() {
  return (  
      <><DefaultLayout></DefaultLayout></>
  );
}

export default App;
