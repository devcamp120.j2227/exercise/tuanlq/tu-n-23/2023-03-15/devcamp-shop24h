
import React from "react"

const Home = React.lazy(() => import('./views/Home/HomePage'));
const Products = React.lazy(() => import("./views/ProductsPages"));
const LoginForm = React.lazy(() => import("./views/LoginUser"));
const RouteList = [
    {path: "/", exact: true ,name: "HomePage", element: Home},
    {path:"/Products", exact: true, name: "Products", element:Products},
    {path:"/Login", exact: true, name:"LogIn", element: LoginForm},
]
export default RouteList