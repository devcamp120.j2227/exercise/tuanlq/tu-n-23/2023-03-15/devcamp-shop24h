import { Card, CardContent, Button, TextField} from "@mui/material";
import { styled } from "@mui/material";
import GoogleIcon from '@mui/icons-material/Google';
import { GoogleAuthProvider, signInWithPopup } from "firebase/auth";
import auth from "../firebase";
import { useNavigate} from 'react-router-dom';
import {useSelector, useDispatch} from "react-redux";
import { useState } from "react";
import { signInGoogleAccountSuccessful } from "../actions/action";



const provider = new GoogleAuthProvider();
const StyledTextfield = {
    width: "100%",
    borderRadius:"20px",
    margin:"15px 0",
    border:"1px solid grey",
    height: "40px",
    padding:"15px"
}
export default function LoginUser (){
    const dispatch = useDispatch()
    const navigate = useNavigate()
    const StyledButton = styled(Button)({
        width:"100%",
        borderRadius: "20px"
    })
    const onClickSignIn = () => {
        signInWithPopup(auth, provider)
            .then((res) => {
                dispatch(signInGoogleAccountSuccessful(res.user));
                navigate("/");
            })
            .catch((error) => {
                console.log(error)
            })
    }
    return(
        <div style={{display: "flex", justifyContent:"center", position:"relative", top:'200px', }}>
            <Card sx={{width:"400px", borderRadius: "10px", backgroundColor:"rgba(132, 132, 132, 0.608)"}}>
                <CardContent style={{
                    display: "flex", 
                    justifyContent:"center", 
                    flexDirection:"column", 
                    alignItems:"center", 
                    boxSizing:"border-box",
                    padding:"50px 40px"
                }}>
                    <StyledButton variant="contained" sx={{ backgroundColor: "rgba(215, 27, 27, 0.79)" }} onClick={onClickSignIn} ><GoogleIcon/>&ensp;Sign In with&ensp;<b>Google</b></StyledButton>
                    <div style={{display:"flex", margin: "35px 0 15px 0", position:"relative", top:"20px"}}>
                        <div style={{backgroundColor:"white", height:"1px", width:"150px"}}/>
                        <p style={{
                            height:"40px",
                            width:"40px",
                            borderRadius:"50%",
                            border:"1px solid white",
                            display:"flex", 
                            justifyContent:"center",
                            alignItems:"center",
                            position:"relative",
                            top:"-19px",
                            fontSize:"20px",
                            color:"white"
                        }}>or</p>
                        <div style={{backgroundColor:"white", height:"1px", width:"150px"}}/>
                    </div>
                    <input placeholder="Username" style={{...StyledTextfield}} onFocus={(e) => e.target.style.outline = "none"}/>
                    <input placeholder="Password" style={{...StyledTextfield}} onFocus={(e) => e.target.style.outline = "none"}/>
                    <StyledButton variant="contained" sx={{backgroundColor:"rgba(65, 215, 27, 0.79)", marginTop:"20px"}} >Sign in</StyledButton>
                </CardContent>
            </Card>
        </div>
    )
}