import { 
    SEND_REQUEST_TAKE_PRODUCT_PENDING,
    SEND_REQUEST_TAKE_PRODUCT_START,
    SEND_REQUEST_TAKE_PRODUCT_END,
    SIGN_OUT_GOOGLE_ACCOUNT,
    SEND_AUTHENTICATION_TO_GOOGLE_SUCCESS
} from "../constant/constant";


export const fetchDataProduct = () => {
    return async (dispatch) => {
        try{
            dispatch({
                type: SEND_REQUEST_TAKE_PRODUCT_PENDING
            })
            var requestOption = {
                method:"GET",
            }
            const responseURL = await fetch ("/product?limit=8", requestOption);

            const data = await responseURL.json();

            await dispatch({
                type: SEND_REQUEST_TAKE_PRODUCT_START,
                data: data
            })
        }
        catch (error){
            return({
                type: SEND_REQUEST_TAKE_PRODUCT_END,
                error: error
            })
        }
    }
}

export const signInGoogleAccountSuccessful = (data) => {
    return{
        type: SEND_AUTHENTICATION_TO_GOOGLE_SUCCESS,
        payload: data
    }
}

export const signOutGoogleAccount = () => {
    return{
        type: SIGN_OUT_GOOGLE_ACCOUNT,
    }
}