//ipmort thư viện mongoose
const mongoose = require ("mongoose");
const productType = require("./productType");

// class Schema từ thư viện mongoose
const Schema = mongoose.Schema;

//khởi tạo instance courseSchema từ class Schema
const productSchema = new Schema({
   name:{
    type: String,
    require: true,
    unique:true
   },
   description:{
    type: String,
   },
   type:{
    type: mongoose.Types.ObjectId,
    ref: "ProductType",
    require: true
   },
   imageURL:{
    type: String,
    require: true,
   },
   buyPrice:{
    type: Number,
    require: true
   },
   promotionPrice:{
    type: Number,
    require: true
   },
   amount:{
    type: Number,
    default: 0
   }
},{
    timestamps:true
});
//biên dịch course model từ courseSchema
module.exports = mongoose.model("Product", productSchema);