//ipmort thư viện mongoose
const mongoose = require ("mongoose");
const productType = require("./productType");

// class Schema từ thư viện mongoose
const Schema = mongoose.Schema;

//khởi tạo instance courseSchema từ class Schema
const customerSchema = new Schema({
    fullName:{
     type: String,
     require: true,
    },
    phone:{
     type: String,
     require: true,
     unique: true
    },
    email:{
        type: String,
        require: true,
        unique: true
    },
    address:{
        type: String,
        default:""
    },
    city:{
        type: String,
        default:""
    },
    country:{
        type: String,
        default:""
    },
    orders:[{
        type: mongoose.Types.ObjectId,
        ref: "Order"
    }]
 },{
     timestamps:true
 });
//biên dịch course model từ courseSchema
module.exports = mongoose.model("Customer", customerSchema);