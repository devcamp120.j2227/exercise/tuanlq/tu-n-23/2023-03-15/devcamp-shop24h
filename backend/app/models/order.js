//import thư viện mongoose
const { default: mongoose } = require("mongoose");

// class Schema từ thư viện mongoose
const Schema = mongoose.Schema;

//khởi tạo instance courseSchema từ class Schema
const orderSchema = new Schema({
    orderDate:{
        type: Date,
        default: Date.now()
    },
    shippedDate:{
        type: Date,
    },
    note:{
        type: String,
    },
    orderDetail:[{
    type: mongoose.Types.ObjectId,
    ref: "Order"
    }],
    cost: {
        type: Number,
        default: 0 
    }
 },{
     timestamps:true
 });
//biên dịch course model từ courseSchema
module.exports = mongoose.model("Order", orderSchema);