//ipmort thư viện mongoose
const mongoose = require ("mongoose");

// class Schema từ thư viện mongoose
const Schema = mongoose.Schema;

//khởi tạo instance courseSchema từ class Schema
const productTypeSchema = new Schema({
   name:{
    type: String,
    require: true,
    unique:true
   },
   description:{
    type: String,
   },
},{
    timestamps:true
});
//biên dịch course model từ courseSchema
module.exports = mongoose.model("Product_Type", productTypeSchema);