// khai báo thư viện express js
const { response } = require("express");
const express = require("express");

//khai báo router app
const router = express.Router();
// Import product middleware
const productTypeMiddleware = require("../middleware/productTypeMiddleware");

//import product controller
const productTypeController = require("../controller/productTypeController")


router.post("/productType", productTypeMiddleware.createProductMiddleware, productTypeController.createProductType);

router.get("/productType", productTypeMiddleware.getAllProductMiddleware, productTypeController.getAllProductType);

router.get("/productType/:productTypeId", productTypeMiddleware.getDetailProductMiddleware, productTypeController.getProductTypeByID );

router.put("/productType/:productTypeId", productTypeMiddleware.updateProductMiddleware, productTypeController.updateProductTypeById);

router.delete("/productType/:productTypeId", productTypeMiddleware.deleteProductMiddleware, productTypeController.deleteProductTypeById);

module.exports = router;