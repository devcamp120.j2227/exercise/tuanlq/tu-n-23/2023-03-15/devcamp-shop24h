// khai báo thư viện express js
const { response } = require("express");
const express = require("express");

//khai báo router app
const router = express.Router();
//import product controller
const customerController = require("../controller/customerController");

router.post("/customer", customerController.createCustomer);

router.get("/customer", customerController.getAllCustomer);

router.get("/customer/:customerId", customerController.getCustomerByID);

router.put("/customer/:customerId", customerController.updateCustomerById);

router.delete("/customer/:customerId", customerController.deleteCustomerById);

module.exports = router;