//import thư viện mongoose
const mongoose = require("mongoose");

//import Review Model
const customerModel = require("../models/Customer");

const createCustomer = (request, response) =>{
     // B1: Chuẩn bị dữ liệu
     const body = request.body;
     // console.log(body)
        //   {
        //     "bodyFullName": "Le Quang Tuan",
        //     "bodyPhone":"0965127997",
        //      "bodyEmail": "maitostaham@gmail.com",
        //      "bodyAddress" : "Thủ Đức",
        //      "bodyCity": "Sài Gòn",
        //      "bodyCountry": "Việt Nam", 
        //  }
 
     // B2: Validate dữ liệu
    if(!body.bodyFullName){
        return response.status(400).json({
            status: "BAD REQUEST",
            message: "FullName không hợp lệ" 
        })
    }
    if(!body.bodyPhone){
        return response.status(400).json({
            status: "BAD REQUEST",
            message: "Phone không hợp lệ" 
        })
    }
    if(!body.bodyEmail){
        return response.status(400).json({
            status: "BAD REQUEST",
            message: "Email không hợp lệ" 
        })
    }
 
     // B3: Thao tác với cơ sở dữ liệu
     const newCustomer = {
        id: mongoose.Types.ObjectId,
        fullName: body.bodyFullName,
        phone:  body.bodyPhone,
        email: body.bodyEmail,
        address: body.bodyAddress,
        city:body.bodyCity,
        country: body.bodyCountry
     }
 
    customerModel.create(newCustomer, (error, data) => {
         if (error) {
             return response.status(500).json({
                 status: "Internal server error",
                 message: error.message
             })
         }  
         return response.status(201).json({
            status: "Create customer type: Successfull",
            data: data
        })
     })
}
const getAllCustomer  = (request, response) =>{
    // Bước 1: chuẩn bị dữ liệu
    // Bước 2: validate dữ liệu
    // bước 3: Gọi Model tạo dữ liệu
    customerModel.find((error,data) => {
        if(error){
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status: "Get all customer: Successfull",
            data: data
        })
    })
}
const getCustomerByID =  (request, response) =>{
    // bước 1: chuẩn bị dữ liệu
    const customerId =  request.params.customerId;
    //bước 2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(customerId)){
     return response.status(400).json({
             status: "Bad request",
             message: "customerId không hợp lệ" 
         })
     }
     //bước 3: gọi model tạo dữ liêu
     customerModel.findById(customerId,(error,data) =>{
         return response.status(200).json({
             status: "Get detail customer successfull",
             data: data
             })
         }
     )  
}
const updateCustomerById =  (request, response) =>{
    // bước 1: chuẩn bị dữ liệu
    const customerId =  request.params.customerId;
    const body = request.body;
    //bước 2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(customerId)){
        return response.status(400).json({
                status: "Bad request",
                message: "CustomerId không hợp lệ" 
            })
        }
        
    if(body.bodyFullName !== undefined && body.bodyFullName.trim() === ""){
        return response.status(400).json({
            status: "BAD REQUEST",
            message: "FullName không hợp lệ" 
        })
    }
    if(body.bodyPhone !== undefined && body.bodyPhone.trim() === ""){
        return response.status(400).json({
            status: "BAD REQUEST",
            message: "Phone không hợp lệ" 
        })
    }
    if(body.bodyEmail !== undefined && body.bodyEmail.trim() === ""){
        return response.status(400).json({
            status: "BAD REQUEST",
            message: "Email không hợp lệ" 
        })
    } 
    const updateCustomer = {
        fullName: body.bodyFullName,
        phone:  body.bodyPhone,
        email: body.bodyEmail,
        address: body.bodyAddress,
        city:body.bodyCity,
        country: body.bodyCountry
    }
    //bước 3: gọi model tạo dữ liêu
    customerModel.findByIdAndUpdate(customerId,updateCustomer,{new: true},(error,data) =>{
            if(error){
                return response.status(500).json({
                    status: "Internal server error",
                    message: error.message
                })
            }
            return response.status(200).json({
                status: "Update customer successfull",
                data: data
                })
            }
        )  
    }
    const deleteCustomerById = (request, response) => {
        //B1: chuẩn bị dữ liệu
        const customerId =  request.params.customerId;
    
        //B2: Validate dữ liệu
        if(!mongoose.Types.ObjectId.isValid(customerId)){
            return response.status(400).json({
                status: "Bad Request",
                message: "Customer ID không hợp lệ"
            })
        } 
        //B3:  Gọi model tạo dữ liệu
        customerModel.findByIdAndDelete(customerId, (error, data)=> {
            if(error){
                return response.status(500).json({
                    status: "Internal server error",
                    message: error.message
                })
            }
            return response.status(204).json({
                status: "Delete customer successfully"
            })
        })
    }
module.exports = {
    createCustomer,
    getAllCustomer,
    getCustomerByID,
    updateCustomerById,
    deleteCustomerById
}