//import thử viện mongoose
const mongoose = require("mongoose");

//import Order model
const orderModel = require("../models/order");
const customerModel  = require("../models/Customer");

const createOrderOfCustomer = (request, response) => {
    //B1:  chuẩn bị dữ liệu
    const body = request.body;
    // {
    //     "bodyShipedDate" : "123",
    //     "bodyNote" : "NoThing", 
    //      "bodyCost": 12000
    // }
    const customerId = request.params.customerId
    //B2: VAlidate dư liệu
    if(!mongoose.Types.ObjectId.isValid(customerId)){
        return response.status(400).json({
            status: "Bad Request",
            message: "Customer ID không hợp lệ"
        })
    }
    const regexDateFormat = /^\d{4}-\d{2}-\d{2}$/;
    if(!body.bodyShipedDate.match(regexDateFormat)){
        return response.status(400).json({
            status: "Bad Request",
            message: "Shiped date format yyyy-mm-dd"
        })
    }
    if ( !body.bodyCost &&(isNaN(body.bodyCost) || body.bodyCost > 0)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "Cost không hợp lệ"
        })
    }
    //B3: Thao tác với cơ sở dữ liệu
    const newOrder = {
        orderDate : body.bodyDate,
        shippedDate: body.bodyShipedDate,
        note : body.bodyNote,
        cost: body.bodyCost
    }
    //B4: kết quả trả về
    orderModel.create(newOrder,(error, data) =>{
       if(error){
        return response.status(500).json({
            status:"Internal server error",
            message: error.message
        })
       }
        // Thêm ID của order mới vào mảng orders của customer đã chọn
        customerModel.findByIdAndUpdate(customerId, {
            $push: {
                orders: data._id
            }
        },{new:true}, (err, data) => {
            if (err) {
                return response.status(500).json({
                    status: "Internal server error",
                    message: err.message
                })
            }

            return response.status(201).json({
                status: "Create Order Successfully",
                data: data
            })
        })
    })
}
const getAllOrder = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    // B2: Validate dữ liệu
    // B3: Gọi Model tạo dữ liệu
    orderModel.find((error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Get all order successfully",
            data: data
        })
    })
}
const getAllOrderOfCustomer = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const customerId = request.params.customerId;

    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(customerId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "Customer ID không hợp lệ"
        })
    }

    // B3: Thao tác với cơ sở dữ liệu
    customerModel.findById(customerId)
        .populate("orders")
        .exec((error, data) => {
            if (error) {
                return response.status(500).json({
                    status: "Internal server error",
                    message: error.message
                })
            }

            return response.status(200).json({
                status: "Get all order of customer successfully",
                data: data
            })
        })
}
const getOrderById = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const orderId = request.params.orderId;

    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "Order ID không hợp lệ"
        })
    }

    // B3: Gọi Model tạo dữ liệu
    orderModel.findById(orderId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Get detail order successfully",
            data: data
        })
    })
}
const updateOrderByID = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const orderID = request.params.orderId;
    const body = request.body;

    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(orderID)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "OrderID không hợp lệ"
        })
    }

    const regexDateFormat = /^\d{4}-\d{2}-\d{2}$/;
    if(!body.bodyShipedDate.match(regexDateFormat)){
        return response.status(400).json({
            status: "Bad Request",
            message: "Shiped date format yyyy-mm-dd"
        })
    }
    if ( body.bodyCost == undefined && (isNaN(body.bodyCost) || body.bodyCost > 0)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "Cost không hợp lệ"
        })
    }
    // B3: Gọi Model tạo dữ liệu
    const newOrder = {
        orderDate : body.bodyDate,
        shippedDate: body.bodyShipedDate,
        note : body.bodyNote,
        cost: body.bodyCost
    }

    orderModel.findByIdAndUpdate(orderID, newOrder, {new:true},(error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Update order successfully",
            data: data
        })
    })
}
const deleteOrderByID = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const customerId = request.params.customerId;
    const orderId = request.params.orderId;

    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(customerId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "Customer Id không hợp lệ"
        })
    }

    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "orderId không hợp lệ"
        })
    }

    // B3: Thao tác với CSDL
    orderModel.findByIdAndDelete(orderId, (error) => {
        if (error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        // Sau khi xóa xong 1 reivew khỏi collection cần cóa thêm reviewID trong course đang chứa nó
        customerModel.findByIdAndUpdate(customerId, {
            $pull: { orders: orderId}
        }, (err, data) => {
            if (err) {
                return response.status(500).json({
                    status: "Internal server error",
                    message: err.message
                })
            }
        })
        return response.status(204).json({
            status: "Delete order successfully"
        })
    })
}
module.exports = {
    createOrderOfCustomer,
    getAllOrder,
    getAllOrderOfCustomer,
    getOrderById,
    updateOrderByID,
    deleteOrderByID,
}