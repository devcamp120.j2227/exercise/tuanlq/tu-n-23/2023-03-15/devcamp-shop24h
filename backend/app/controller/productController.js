//import thư viện mongoose
const { response } = require("express");
const mongoose = require("mongoose");

//import Review Model
const productModel = require("../models/product");

// call back function create product 
const createProduct = (request, response) =>{
    //B1: chuẩn bị dữ liệu
    const body = request.body;
    // {
    //     "bodyName": "Thuốc",
    //     "bodyDescription": "Thuốc trị đau đầu",
    //     "bodyType": "6375edad2bd7724ea38ce201",
    //     "bodyImageURL": "https://i-cf65.ch-static.com/content/dam/cf-consumer-healthcare/panadol/vi_vn/vietnamproduct/panadol_regular_pack_shot_blue/product_detail/Desktop-455x455.png?auto=format",
    //     "bodyBuyPrice" : 60000,
    //     "bodyPromotionPrice": 56000,
    //     "bodyAmount": 60 
    // }
    //B2: validate dữ liệu
    if(!body.bodyName){
        return response.status(400).json({
            status: "BAD REQUEST",
            message: "Name không hợp lệ" 
        })
    }
    if(!body.bodyType){
        return response.status(400).json({
            status: "BAD REQUEST",
            message: "Type không hợp lệ" 
        })
    }
    if(!body.bodyImageURL){
        return response.status(400).json({
            status: "BAD REQUEST",
            message: "Image URL không hợp lệ" 
        })
    }
    if(!body.bodyBuyPrice){
        return response.status(400).json({
            status: "BAD REQUEST",
            message: "BuyPrice không hợp lệ" 
        })
    }
    if(!body.bodyPromotionPrice){
        return response.status(400).json({
            status: "BAD REQUEST",
            message: "promotionPrice không hợp lệ" 
        })
    }
    //B3: thao tác cơ sở dữ liệu
    const newProduct = {
        name: body.bodyName,
        description: body.bodyDiscription,
        type: body.bodyType,
        imageURL: body.bodyImageURL,
        buyPrice: body.bodyBuyPrice,
        promotionPrice: body.bodyPromotionPrice,
        amount: body.bodyAmount
    }
    productModel.create(newProduct, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }  
        return response.status(201).json({
           status: "Create product : Successfull",
           data: data
       })
    })
}
const getAllProduct = (request, response) =>{
     // Bước 1: chuẩn bị dữ liệu
    // Bước 2: validate dữ liệu
    // bước 3: Gọi Model tạo dữ liệu
    productModel.find().limit(8).exec((error,data) => {
        if(error){
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status: "Get all product: Successfull",
            data: data
        })
    })
}
const getProductByID =  (request, response) =>{
    // bước 1: chuẩn bị dữ liệu
    const productID =  request.params.productId;
    //bước 2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(productID)){
     return response.status(400).json({
             status: "Bad request",
             message: "productTypeId không hợp lệ" 
         })
     }
     //bước 3: gọi model tạo dữ liêu
     productModel.findById(productID,(error,data) =>{
         return response.status(200).json({
             status: "Get detail product  successfull",
             data: data
             })
         }
     )  
 }
const updateProductById =  (request, response) =>{
// bước 1: chuẩn bị dữ liệu
const productID =  request.params.productId;
const body = request.body;
//bước 2: Validate dữ liệu
if(!mongoose.Types.ObjectId.isValid(productID)){
    return response.status(400).json({
            status: "Bad request",
            message: "ProductId không hợp lệ" 
        })
    }  
    if(body.bodyName !== undefined && body.bodyName.trim() === ""){
        return response.status(400).json({
            status: "BAD REQUEST",
            message: "Name không hợp lệ" 
        })
    }
    if(body.bodyType !== undefined && body.bodyType.trim() === ""){
        return response.status(400).json({
            status: "BAD REQUEST",
            message: "Type không hợp lệ" 
        })
    }
    if(body.bodyImageURL !== undefined && body.bodyImageURL.trim() === ""){
        return response.status(400).json({
            status: "BAD REQUEST",
            message: "Image URL không hợp lệ" 
        })
    }
    if(body.bodyBuyPrice !== undefined && (isNaN(body.bodyBuyPrice) || body.bodyBuyPrice < 0)){
        return response.status(400).json({
            status: "BAD REQUEST",
            message: "BuyPrice không hợp lệ" 
        })
    }
    if(body.bodyPromotionPrice !== undefined && (isNaN(body.bodyPromotionPrice) || body.bodyPromotionPrice < 0)){
        return response.status(400).json({
            status: "BAD REQUEST",
            message: "promotionPrice không hợp lệ" 
        })
    }
    const updateProduct = {
        name: body.bodyName,
        description: body.bodyDiscription,
        type: body.bodyType,
        image: body.bodyImageURL,
        buyPrice: body.bodyBuyPrice,
        promotionPrice: body.bodyPromotionPrice,
        amount: body.bodyAmount
    }
    //bước 3: gọi model tạo dữ liêu
    productModel.findByIdAndUpdate(productID,updateProduct,{new: true},(error,data) =>{
        if(error){
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status: "Update product  successfull",
            data: data
            })
        }
    )  
}
const deleteProductById = (request, response) => {
    //B1: chuẩn bị dữ liệu
    const productID = request.params.productId;

    //B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(productID)){
        return response.status(400).json({
            status: "Bad Request",
            message: "Product ID không hợp lệ"
        })
    } 
    //B3:  Gọi model tạo dữ liệu
    productModel.findByIdAndDelete(productID, (error, data)=> {
        if(error){
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(204).json({
            status: "Delete product successfully"
        })
    })
}
const getProductWithLimit = (request, response) => {
    //B1: Chuẩn bị dữ liệu
    const limit = request.query.limit;
    //Bước2: kiểm tra dữ liệu
    if( limit < 0){
        return response.status(400).json({
            status:"Bad request",
            message: "Limit is not valid!"
        })
    }
    //B3: Lấy dữ liệu trả về 
    if(limit !== undefined){
        productModel.find().limit(limit).exec((error, data) => {
            if(error){
                return response.status(500).json({
                    status: "Internal server error!",
                    message: error.message
                })
            }
            return response.status(201).json({
                status: "Get Product successfull!",
                data: data
            })
        })
    }else{
        productModel.find().exec((error, data) => {
            if(error){
                return response.status(500).json({
                    status: "Internal server error!",
                    message: error.message
                })
            }
            return response.status(201).json({
                status: "Get Product successfull!",
                data: data
            })
        })
    }
}
module.exports= {
    createProduct,
    getAllProduct,
    getProductByID,
    updateProductById,
    deleteProductById,
    getProductWithLimit
}