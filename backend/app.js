// khai báo thư viện express 
const { response, request } = require("express");
const express = require ("express");
var cors = require('cors')

// khai báo mongoose 
var mongoose = require('mongoose');
mongoose.set('strictQuery', true);
//khởi tạo app express
const app = express();

//khai báo cổng chạy app
const port= 8000;
//khai báo router 
const productTypeRouter = require("./app/routes/productTypeRouter");
const productRouter = require("./app/routes/productRouter");
const customerRouter = require("./app/routes/customerRouter");
const orderRouter = require("./app/routes/orderRouter")
const orderDetailRouter = require("./app/routes/orderDetailRouter");
//cấu hình request đọc 
app.use(express.json());
//khai báo model
const productTypeModel = require("./app/models/productType");
const productModel = require("./app/models/product");
const customerModel = require("./app/models/Customer");
const orderModel = require("./app/models/order")
const orderDetailModel = require("./app/models/orderDetail");
app.post("/post", (req, res) => {
    console.log("Connected to React");
    res.redirect("/");
  });
//kết nối với mongoDB
mongoose.connect('mongodb://127.0.0.1:27017/CRUD_shop24h', function(error){
    if(error) throw error;
    console.log('Successfully connected');
});
// App sử dụng router
app.use("/", productTypeRouter);
app.use("/", productRouter);
app.use("/", customerRouter);
app.use("/", orderRouter);
app.use("/", orderDetailRouter);
app.use(cors())
// chạy app trên cổng
app.listen(port,()=>{
    console.log("App listening on port :" , port)
})